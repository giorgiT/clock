import React from 'react';
import './App.css'

export default class hello extends React.Component {

    constructor(props) {
		
		super(props)
		
		this.state = {
			time: new Date()
		}
		
	}
	
	componentDidMount() {
		
		setInterval(this.update, 1000)
		
	}
	
	update = () => {
		
		this.setState({
			time: new Date()
		})
		
	};


  render() {

    const h = this.state.time.getHours() + 4;
    const m = this.state.time.getMinutes()
    const s = this.state.time.getSeconds()

      return (
          <div className="first-clock">
  <h1 >{h % 12}:{(m < 10 ? '0' + m : m)}:{(s < 10 ? '0' + s : s)} {h < 12 ? 'am' : 'pm'}
   </h1> 
   <h2 className="Usa">United States Of America</h2>
          </div>
        
      )
  }


}

