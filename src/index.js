import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Hello from './App.jsx';

class Clock extends React.Component {
	
	constructor(props) {
		
		super(props)
		
		this.state = {
			time: new Date()
		}
		
	}
	
	componentDidMount() {
		
		setInterval(this.update, 1000)
		
    }
    
    componentWillUnmount(){
        clearInterval(); /* ai es 2 xazi ar ewera kodshi romelic davakopire da saciroa da ramdenad
        sworad mimweria da toLocaleTimeString romaaa sad da rodis unda gamoviyeno aq ar maqvs gamoyenebuli
        da zogadad ramdenad sworadaa es kodi leqciis dros daweril kodtan shedarebit
        */
    }
	
	update = () => {
		
		this.setState({
			time: new Date()
		})
		
	};
	
	render() {
		
		const h = this.state.time.getHours()
		const m = this.state.time.getMinutes()
		const s = this.state.time.getSeconds()
		
		return (
            <div className="second-clock">

            <Hello/>
		
          <h1 className>{h % 12}:{(m < 10 ? '0' + m : m)}:{(s < 10 ? '0' + s : s)} {h < 12 ? 'am' : 'pm'}</h1>
          <h2 className="Georgia">Georgia</h2>
            </div>
		
		)
		
	}
	
}

ReactDOM.render(<Clock /> ,
    document.getElementById('root'))
// -------------------------------------------------------------------
// -----------toLocaleString ar gaoimyenebia da rato?--------------------------------------------------------

